# Data Modeling

## Scenario Outline

You have been asked by a client to design and implement a new data mart to help them understand the profitability of their sales channel. They are particularly interested in understanding which members of their sales team are their top performers both by volume and profitability.

## Recommended architecture

What database system do you recommend? Why do you recommend this system?


##Current state

Initial discovery exercises with the client�s stakeholders have identified the following tables as being of potential interest.

###Tables
* Goal mapping v3
* HREmployees
* HRCorporateLocations
* Orders all v3
* Order items all v3
* tblInventoryCategories
* tblPriceList
* AccountingProfitModel2019

Full schemas of these tables are attached with this exercise.

##KPI definition

What KPIs would you recommend for the client?
 
##Recommend a data model

What data model would you recommend for the client? Describe both the model selected and the reasons for choosing this model.
 
##Implement the data model
Write the SQL statements to generate tables you are recommending.

Write SQL statements to populate your tables.
 
##Solve the business problem
Write SQL statements to be used by the analytics team to identify top performers by volume and profitability.